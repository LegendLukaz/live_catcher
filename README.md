# Live Catcher

An automation script for fetching latest live stream from a channel and download it using youtube-dl. A timer can be set to only start fetching the live stream near the starting time